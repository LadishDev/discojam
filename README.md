<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">DiscoJam</h3>

<div align="center">

[![Status](https://badgen.net/badge/status/active/green)]()
[![Last Commit](https://badgen.net/gitlab/last-commit/ladishdev/discojam)](https://gitlab.com/ladishdev/discojam)

[![GitLab Issues](https://badgen.net/gitlab/issues/LadishDev/discojam)](https://gitlab.com/LadishDev/discojam/issues)
[![GitLab Merge Requests](https://badgen.net/gitlab/mrs/ladishdev/discojam)](https://gitlab.com/ladishdev/discojam/merge_requests)
[![License](https://badgen.net/gitlab/license/ladishdev/discojam)](/LICENSE)

</div>

---

<p align="center"> A little Application that utilizes the Discogs API to let users explore and save different music
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

This project is to be able to bring music together in one application so that users can find different songs that they might be interested in on the app. They then can save what they have found to come back and look at later or explore more about what they are looking at. Using such links provided that can take you to the discogs site to see more. 

I plan to add a lot more functionality to this app to have more data within and links to services so I can make it a one all app to find out everything they need.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them.

#### .env file

In order to use this program you will need your own access to the API to create 2 required enviroment variables. These variables should be User-Agent & Authorization. Create a .env file within the data folder within the bin one.

The file content should look like this

```
User-Agent = "   " // Application Name Goes in here
Authorization = "Discogs token= " // Add your token to this
```

### Installing


```
Complile the code using OpenFrameworks and run the exe and make sure you have enabled the addon of OfxJson.
```

Now you can use the application as intended to search and browse music.

## 🎈 Usage <a name="usage"></a>

You are able to search for songs to find info on them. U can use the secret search feature [Artist Name - Song Name] to find a specific song. 

You can then save them so u can come back to them whenever you wish or click on the view more button to take you to Discogs to view more information.

## ⛏️ Built Using <a name = "built_using"></a>

- [DiscogsAPI](https://www.discogs.com/developers) - API Data
- [OpenFrameworks](https://openframeworks.cc) - Application Framework

## ✍️ Authors <a name = "authors"></a>

- [@LadishDev](https://gitlab.com/ladishdev) - Full Scope of this Project


## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- Thank you to Discogs for access and use of your API
